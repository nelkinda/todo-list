import React from 'react';
import {render} from 'react-dom';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {todolists: []};
    }

    fetchTodoListsFromBackend() {
        var request = new XMLHttpRequest();
        request.addEndEventListener("load",
            (event) => (this.setState({todolists: JSON.parse(request.responseText)})));
        request.open("GET", 'http://localhost:3000/lists');
        request.send();
    }

    componentWillMount() {
        // this.fetchTodoListsFromBackend();
        this.setState({
            todolists: [
                {name: "Douglas"},
                {name: "Surabhi"},
                {name: "Vivek"}
            ]
        })
    }

    render() {
        return (
            <div>
                <h2>Your Todo Lists</h2>
                <ul>
                    {this.state.todolists.map(
                        (todolist, i) =>
                            <li key={i}>
                                {todolist.name}
                            </li>

                    )}
                </ul>
            </div>
        )
    }

}

render(<App/>, document.getElementById('container'));